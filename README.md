# Payer Portal

## Getting Started

To get you started you can simply clone the payer-portal-ui repository and install the dependencies:

### Prerequisites

You need git to clone the payer-portal-ui repository. You can get git from
[http://git-scm.com/](http://git-scm.com/).

We also use a number of node.js tools to initialize and test angular-seed. You must have node.js and
its package manager (npm) installed.  You can get them from [http://nodejs.org/](http://nodejs.org/).

### Clone angular-seed

Clone the angular-seed repository using [git][git]:

```
git clone https://bitbucket.org/lcpatientportal/payer-portal-ui.git
cd payer-portal-ui
```

### Install Dependencies

We have two kinds of dependencies in this project: tools and angular framework code.  The tools help
us manage and test the application.

* We get the tools we depend upon via `npm`, the [node package manager][npm].
* We get the angular code via `bower`, a [client-side code package manager][bower].

We have preconfigured `npm` to automatically run `bower` so we can simply do:

```
npm install
```

Behind the scenes this will also call `bower install`.  You should find that you have two new
folders in your project.

* `node_modules` - contains the npm packages for the tools we need
* `app/bower_components` - contains the angular framework files

*Note that the `bower_components` folder would normally be installed in the root folder but
payer-portal-ui changes this location through the `.bowerrc` file.  Putting it in the app folder makes
it easier to serve the files by a webserver.*

### Run the Application

We have preconfigured the project with a simple development web server.  The simplest way to start
this server is:

```
npm start
```

Now browse to the app at `http://localhost:8000/index.html`.



## Directory Layout

```
app/                    --> all of the source files for the application
  app.css               --> default stylesheet
  components/           --> all app specific modules
    version/              --> version related components
      version.js                 --> version module declaration and basic "version" value service
      version_test.js            --> "version" value service tests
      version-directive.js       --> custom directive that returns the current app version
      version-directive_test.js  --> version directive tests
      interpolate-filter.js      --> custom interpolation filter
      interpolate-filter_test.js --> interpolate filter tests
  view1/                --> the view1 view template and logic
    view1.html            --> the partial template
    view1.js              --> the controller logic
    view1_test.js         --> tests of the controller
  view2/                --> the view2 view template and logic
    view2.html            --> the partial template
    view2.js              --> the controller logic
    view2_test.js         --> tests of the controller
  app.js                --> main application module
  index.html            --> app layout file (the main html template file of the app)
  index-async.html      --> just like index.html, but loads js files asynchronously
karma.conf.js         --> config file for running unit tests with Karma
e2e-tests/            --> end-to-end tests
  protractor-conf.js    --> Protractor config file
  scenarios.js          --> end-to-end scenarios to be run by Protractor
```

## Testing

There are two kinds of tests in the payer-portal-ui application: Unit tests and end-to-end tests.

### Running Unit Tests

The payer-portal-ui app comes preconfigured with unit tests. These are written in
[Jasmine][jasmine], which we run with the [Karma Test Runner][karma]. We provide a Karma
configuration file to run them.

* the configuration is found at `karma.conf.js`
* the unit tests are found next to the code they are testing and are named as `..._test.js`.

The easiest way to run the unit tests is to use the supplied npm script:

```
npm test
```

This script will start the Karma test runner to execute the unit tests. Moreover, Karma will sit and
watch the source and test files for changes and then re-run the tests whenever any of them change.
This is the recommended strategy; if your unit tests are being run every time you save a file then
you receive instant feedback on any changes that break the expected code functionality.

You can also ask Karma to do a single run of the tests and then exit.  This is useful if you want to
check that a particular version of the code is operating as expected.  The project contains a
predefined script to do this:

```
npm run test-single-run
```


### End to end testing

The payer-portal-ui app comes with end-to-end tests, again written in [Jasmine][jasmine]. These tests
are run with the [Protractor][protractor] End-to-End test runner.  It uses native events and has
special features for Angular applications.

* the configuration is found at `e2e-tests/protractor-conf.js`
* the end-to-end tests are found in `e2e-tests/scenarios.js`

Protractor simulates interaction with our web app and verifies that the application responds
correctly. Therefore, our web server needs to be serving up the application, so that Protractor
can interact with it.

```
npm start
```

In addition, since Protractor is built upon WebDriver we need to install this.  The payer-portal-ui
project comes with a predefined script to do this:

```
npm run update-webdriver
```

This will download and install the latest version of the stand-alone WebDriver tool.

Once you have ensured that the development web server hosting our application is up and running
and WebDriver is updated, you can run the end-to-end tests using the supplied npm script:

```
npm run protractor
```

This script will execute the end-to-end tests against the application being hosted on the
development server.


## Updating Payer Portal

Previously we recommended that you merge in changes to payer-portal-ui into your own fork of the project.
Now that the Payer Portal library code and tools are acquired through package managers (npm and
bower) you can use these tools instead to update the dependencies.

You can update the tool dependencies by running:

```
npm update
```

This will find the latest versions that match the version ranges specified in the `package.json` file.

You can update the Payer Portal dependencies by running:

```
bower update
```

This will find the latest versions that match the version ranges specified in the `bower.json` file.


## Loading Angular Asynchronously

The angular-seed project supports loading the framework and application scripts asynchronously.  The
special `index-async.html` is designed to support this style of loading.  For it to work you must
inject a piece of Angular JavaScript into the HTML page.  The project has a predefined script to help
do this.

```
npm run update-index-async
```

This will copy the contents of the `angular-loader.js` library file into the `index-async.html` page.
You can run this every time you update the version of Angular that you are using.


## Serving the Application Files

While angular is client-side-only technology and it's possible to create angular webapps that
don't require a backend server at all, we recommend serving the project files using a local
webserver during development to avoid issues with security restrictions (sandbox) in browsers. The
sandbox implementation varies between browsers, but quite often prevents things like cookies, xhr,
etc to function properly when an html page is opened via `file://` scheme instead of `http://`.


### Running the App during Development

The payer-portal-ui project comes preconfigured with a local development webserver.  It is a node.js
tool called [http-server][http-server].  You can start this webserver with `npm start` but you may choose to
install the tool globally:

```
sudo npm install -g http-server
```

Then you can start your own development web server to serve static files from a folder by
running:

```
http-server -a localhost -p 8000
```

Alternatively, you can choose to configure your own webserver, such as apache or nginx. Just
configure your server to serve the files under the `app/` directory.


### Running the App in Production

This really depends on how complex your app is and the overall infrastructure of your system, but
the general rule is that all you need in production are all the files under the `app/` directory.
Everything else should be omitted.

Angular apps are really just a bunch of static html, css and js files that just need to be hosted
somewhere they can be accessed by browsers.

If your Angular app is talking to the backend server via xhr or other means, you need to figure
out what is the best way to host the static files to comply with the same origin policy if
applicable. Usually this is done by hosting the files by the backend server or through
reverse-proxying the backend server(s) and webserver(s).


## Continuous Integration
TODO

## Gulp Process

### Gulp
Gulp solves the problem of repetition. Many of the tasks that web developers find themselves doing over and over on a daily basis can be simplified by becoming automated. Automating repetitive tasks = more time to do non repetitive tasks = more productivity.
Gulp is a javascript task runner that lets you automate tasks such as :

* Bundling and minifying libraries and stylesheets.
* Refreshing your browser when you save a file.
* Quickly running unit tests
* Running code analysis
* Less/Sass to CSS compilation
* Copying modified files to an output directory

#### Gulp file can be found in our project and the file name is gulpfile.js
Gulp has a principal default task, which will run when you type work gulp in terminal, and this task can have some dependency tasks or not, and then all code/secondary tasks
will be executed.
Sintax:
```
gulp.task("default", ["dependency Tasks"], function (callback) {
    //code or task to be executed after dependency tasks
}
```
We have some standalone tasks defined:
1) help - List the available gulp tasks
2) default - displays all the tasks
3) production-release - Task for Bamboo. Will create run production-build, and zip the assets.
4) stage-release - Task for Bamboo. Will create run stage-build, and zip the assets.
5) development-release - Task for Bamboo. Will create run development-build, and zip the assets.
6) production-build - Main task that creates a distribution build
7) stage-build - Main task that creates a distribution build using staging config
8) development-build - Main task that creates a distribution build using development config
9) local-build - Main task that injects all the needed dependencies and uses standard config file
10) build - Task to build everything.
11) prod-build - Alias task for production-build
12) production-zip - Task to set production env variables and zip the "dist" folder content
13) prod-zip - Alias task for production-zip
14) pre-production - Task to setup env variables for production.
15) pre-stage - Task to setup env variables for stage.
16) pre-development - Task to setup env variables for development.
17) pre-local - Task to setup env variables for local.
18) serve-distribution - Starts a web server pointing to the distribution folder. This tasks assumes you already ran one of the -build tasks to generate the ./dist content.
19) serve-local - Starts a web server pointing to the development folder
20) zip - Task to zip the whole /dist folder
21) clean - clean the prod destination folder before a new build
22) temp-clean - clean the temp folder
23) styles - convert and compress less files to css file, and puts it in the ./tmp folder
24) assets - Copy assets (fonts / favIcon / data / images)
25) inject - Inject dependencies in index.html
26) wiredep - Wire-up the bower and application dependencies
27) optimize - Process JavaScript files - bundling and minification
28) templatecache - Create $templateCache from the html templates
29) test - Run unit tests
30) clean-tests - Clean coverage folder
31) lint - Run JSHint (Lint) on all js files in the src/app folder

## Contact

For more information on AngularJS please check out http://angularjs.org/

[git]: http://git-scm.com/
[bower]: http://bower.io
[npm]: https://www.npmjs.org/
[node]: http://nodejs.org
[protractor]: https://github.com/angular/protractor
[jasmine]: http://jasmine.github.io
[karma]: http://karma-runner.github.io
[travis]: https://travis-ci.org/
[http-server]: https://github.com/nodeapps/http-server
