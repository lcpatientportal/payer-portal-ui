//jshint strict: false
module.exports = function(config) {
  config.set({

    basePath: 'src/app',

    files: [
      'src/bower_components/jquery/dist/jquery.js',
      'src/bower_components/angular/angular.js',
      'src/bower_components/angular-animate/angular-animate.js',
      'src/bower_components/angular-bootstrap/ui-bootstrap.min.js',
      'src/bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
      'src/bower_components/ng-table/dist/ng-table.js',
      'src/bower_components/angular-loader/angular-loader.js',
      'src/bower_components/angular-mocks/angular-mocks.js',
      'src/bower_components/angular-messages/angular-messages.js',
      'src/bower_components/angular-route/angular-route.js',
      'src/bower_components/angular-sanitize/angular-sanitize.js',
      'src/bower_components/angular-spinners/dist/angular-spinners.js',
      'src/bower_components/angular-strap/dist/angular-strap.js',
      'src/bower_components/angular-strap/dist/angular-strap.tpl.min.js',
      'src/bower_components/angular-ui-router/release/angular-ui-router.js',
      'src/bower_components/angular-ui-mask/dist/mask.min.js',
      'src/app/**/*.module.js',
      'src/app/**/*.js',
      'src/app/**/*.html'
    ],

    preprocessors: {
      '**/*.html': ['ng-html2js']
    },

    ngHtml2JsPreprocessor: {
      // strip this from the file path
      stripPrefix: 'src/',
      // create a single module that contains templates from all the files
      moduleName: 'templates'
    },

    autoWatch: true,

    colors: true,

    //Possible values:  config.LOG_DISABLE, config.LOG_ERROR, config.LOG_WARN, config.LOG_INFO, config.LOG_DEBUG
    logLevel: config.LOG_ERROR,

    frameworks: ['jasmine'],

    browsers: ['PhantomJS'],

    singleRun: false,

    plugins: [
      'karma-*',
      'phantomjs*'
    ],

    junitReporter: {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    },

    // the default configuration
    htmlReporter: {
      outputDir: 'karma_html' // where to put the reports
    },

    phantomjsLauncher: {
      // Have phantomjs exit if a ResourceError is encountered (useful if karma exits without killing phantom)
      exitOnResourceError: true
    }

  });
};
