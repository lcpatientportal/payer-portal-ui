/**
 * Created by lticrea on 7/29/2016.
 */
/*
 This file holds all the automated tasks we want to run using Gulps

 1) for production environment
 -run: gulp distribution-build
 -run: gulp production-zip

 2) for dev/qa environment  - poc
 - run: gulp development
 - run : gulp development-env
 -copy content of dist folder on the sandbox folder
 */

'use strict';
// load libraries and dependencies
var pkg = require('./package.json');
var args = require('yargs').argv;
var gulp = require('gulp');
var merge = require('merge-stream');
var $ = require('gulp-load-plugins')({ lazy: true });
var del = require('del');
var runSequence = require('run-sequence');

// unit testing dependencies
var wiredep = require('wiredep');
var karmaServer = require('karma').Server;

var srcPath = './src/';
var tmpPath = srcPath + '.tmp/';

/**
 * Config variable
 */
var config = {
  paths: {
    src: {
      root: srcPath,
      jsFiles: srcPath + 'app/**/*.js',
      images: srcPath + 'images/**/*',
      less: srcPath + 'styles/**/*.less',
      fonts: [
      /**
       * OpenSans fonts
       */
        //srcPath + 'fonts/**/OpenSans-Regular-webfont.*',
      /**
       * Bootstrap glyphicons fonts
       */
        srcPath + 'bower_components/bootstrap/fonts/*.*',
      /**
       * Font Awesome fonts
       */
        srcPath + 'bower_components/fontawesome/fonts/*.*'
      ],
      favIcon: srcPath + 'favicon.ico',
      data: srcPath + 'data/**/*',
      bowerFolder: srcPath + 'bower_components',
      templates: srcPath + 'app/**/*.html'
    },
    dest: {
      build: './build/',
      distribution: './dist/',
      tmp: tmpPath,
      templateCache: tmpPath + 'templates.js',
      styleCache: tmpPath + 'style.css'
    }
  },
  configFiles: {
    karma: '/karma.conf.js'
  },
  index: srcPath + 'index.html',
  templateCache: {
    file: 'templates.js',
    options: {
      module: 'payerPortal',
      root: srcPath + 'app/',
      standAlone: false,
      transformUrl: function(url) {
        return url.replace(/\\/g, '/').replace(srcPath, '')// we remove the ./src from the path, making it relative to the src folder
      }
    }
  },
  js: [
    srcPath + 'app/**/*.module.js',
    srcPath + 'app/**/*.js',
    '!**/gulpfile.js',
    '!' + srcPath + 'app/**/*.spec.js',
  ],
  jsOrder: [
    '**/app.module.js',
    '**/*.module.js',
    '**/*.js'
  ],
  /**
   * Tasks that appear in the Main Tasks list when running: gulp
   */
  mainTasks: [
    'distribution-release',
    'distribution-build',
    'serve-distribution',
    'serve-local',
    'test',
    'lint'
  ],
  /**
   * Tasks that appear in the Sub-Tasks list when running: gulp
   */
  subTasks: [
    'temp-clean',
    'templatecache',
    'inject',
    'styles',
    'wiredep'
  ]
}

/**
 * This variable will be populated on first task, and used in all sub-tasks.
 */
var env = {
  path: config.paths.dest.distribution,
  /**
   * Option to minify and group all files
   */
  optimize: true,
  envType: 1,
  js: config.js,
  zipFileName: pkg.name + '.zip',
  /**
   * Variable use to specify the the path of the app. (e.g. "ui" for https://patient.labcorp.com/ui )
   */
  virtualFolder: ''
}

/**
 * List the available gulp tasks
 */
gulp.task('help', $.taskListing.withFilters(function(task) { // subtaskFilter
  return config.subTasks.indexOf(task) >= 0;
}, function(task) { // excludeFilter
  return config.mainTasks.indexOf(task) < 0 && config.subTasks.indexOf(task) < 0;
}));

/**
 * Make 'help' the default task, so when the user runs 'gulp', it will display all the tasks
 */
gulp.task('default', ['help']);

/* Production tasks */

/////////////////////////////////////////////////////////////////////////////////////
//                          RELEASE TASKS
/////////////////////////////////////////////////////////////////////////////////////

/**
 * Task for Bamboo. Will create run distribution-build, and zip the assets.
 */
gulp.task('distribution-release', ['distribution-build'], function(done) {
  runSequence('zip', function() {
    done();
  });
});

/**
 * Task to set production env variables and zip the "dist" folder content
 */
gulp.task('production-zip', ['pre-distribution'], function(done) {
  runSequence('zip', function() {
    done();
  });
});

/**
 * Alias task for production-zip
 */
gulp.task('prod-zip', ['production-zip']);



/////////////////////////////////////////////////////////////////////////////////////
//                          BUILD TASKS
/////////////////////////////////////////////////////////////////////////////////////

/**
 * Main task that creates a distribution build
 */
gulp.task('distribution-build', ['pre-distribution'], function(done) {
  log('Running distribution build task!');

  runSequence('build', function() {
    done();
  });
});

/**
 * Main task that injects all the needed dependencies and uses standard config file
 */
gulp.task('local-build', ['pre-local'], function(done) {
  log('Running local build task!');

  runSequence('inject', function() {
    done();
  });
});


/**
 * Task to build everything.
 */
gulp.task('build', ['clean', 'optimize']);

/////////////////////////////////////////////////////////////////////////////////////
//               ENVIRONMENT VARIABLE SETUP SUB-TASKS
/////////////////////////////////////////////////////////////////////////////////////

/**
 * Task to setup env variables for distribution.
 */
gulp.task('pre-distribution', function(done) {
  log('Setting up production build variables!');
  env.optimize = true;
  env.virtualFolder = 'ui/';
  env.path = config.paths.dest.distribution + env.virtualFolder;
  env.zipFileName = pkg.name + '.zip';

  done();
});

/**
 * Task to setup env variables for local.
 */
gulp.task('pre-local', function(done) {
  log('Setting up local build variables!');

  env.optimize = false;

  done();
});


/////////////////////////////////////////////////////////////////////////////////////
//               SERVER STARTING TASKS
/////////////////////////////////////////////////////////////////////////////////////

/**
 * Starts a web server pointing to the distribution folder.
 * This tasks assumes you already ran one of the -build tasks to generate the ./dist content.
 */
gulp.task('serve-distribution', function(done) {
  log('Starting local server using production folder.');

  return gulp
    .src(config.paths.dest.distribution)
    .pipe(
      $.webserver({
        livereload: false,
        open: true,
        port: 8000,
        fallback: 'index.html'
      }));
});

/**
 * Starts a web server pointing to the development folder
 */
gulp.task('serve-local', ['local-build'], function(done) {
  log('Starting local server using development folder.');

  log('Watching for .less file changes.');
  // will watch on .less file changes, and compile them to .css
  gulp.watch([config.paths.src.less], ['styles']);

  var options = {
    livereload: false,
    open: true,
    port: 8000,
    fallback: 'index.html'
  };
  if (args.useDevice) {
    options.host = '0.0.0.0';
  }

  return gulp
    .src(config.paths.src.root)
    .pipe($.webserver(options));
});

/////////////////////////////////////////////////////////////////////////////////////
//                              OTHER TASKS
/////////////////////////////////////////////////////////////////////////////////////

/**
 * Task to zip the whole /dist folder
 */
gulp.task('zip', function() {
  return gulp.src([env.path + '../**/*', '!**/*.zip'])
             .pipe($.zip(env.zipFileName))
             .pipe(gulp.dest(env.path + 'builds'));
});

/**
 * clean the prod destination folder before a new build
 */
gulp.task('clean', ['temp-clean'], function(done) {
  return clean(env.path + "**/*", done);
});

/**
 * clean the temp folder
 */
gulp.task('temp-clean', function(done) {
  return clean(config.paths.dest.tmp + "**/*", done);
});

/**
 *  convert and compress less files to css file, and puts it in the ./tmp folder
 */
gulp.task('styles', function() {
  return gulp.src(config.paths.src.less)
             .pipe($.less())
             .pipe($.if(env.optimize, $.minifyCss({ compatibility: 'ie8' })))
             .pipe($.concat('style.css'))
             .pipe(gulp.dest(config.paths.dest.tmp));
});


/**
 *  Copy assets (fonts / favIcon / data / images)
 */
gulp.task('assets', function() {
  log('Copying assets (fonts / favIcon / data / images)');

  // copy fonts to destination
  var fonts = gulp
    .src(config.paths.src.fonts)
    .pipe(gulp.dest(env.path + 'fonts/'));

  var favIcon = gulp
    .src(config.paths.src.favIcon)
    .pipe(gulp.dest(env.path));

  var data = gulp
    .src(config.paths.src.data)
    .pipe(gulp.dest(env.path + 'data'));

  var images = gulp.src(config.paths.src.images)
                   .pipe($.imagemin({ optimizationLevel: 5 }))
                   .pipe(gulp.dest(env.path + 'images'));


  return merge(fonts, favIcon, data, images);
});

/**
 * Inject dependencies in index.html
 */
gulp.task('inject', ['wiredep', 'styles', 'templatecache'], function() {

  return gulp.src(config.index)
             .pipe(inject(config.paths.dest.templateCache, 'templates')) // injects templatecache
             .pipe(inject(config.paths.dest.styleCache)) // injects the style.css
             .pipe(injectBase())
             .pipe(gulp.dest(config.paths.src.root));
});

/**
 * Wire-up the bower and application dependencies
 * @return {Stream}
 */
gulp.task('wiredep', function() {
  log('Wiring the bower dependencies into the html');

  var wireStream = wiredep.stream;

  var options = getWiredepDefaultOptions();

  return gulp
    .src(config.index)
    .pipe(wireStream(options))
    .pipe(inject(env.js, '', config.jsOrder))
    .pipe(gulp.dest(config.paths.src.root));
});

/**
 * Process JavaScript files - bundling and minification
 */
gulp.task('optimize', ['inject', 'assets'], function() {
  log('Optimizing the js, css, and html.');

  var assets = $.useref.assets({ searchPath: config.paths.src.root });

  // Filters are named for the gulp-useref path
  var cssFilter = $.filter(['**/*.css'], { restore: true });
  var jsAppFilter = $.filter(['**/app.js'], { restore: true });
  var jslibFilter = $.filter(['**/lib.js'], { restore: true });

  return gulp
    .src(config.index)
    .pipe($.plumber())
    .pipe(assets) // Gather all assets from the html with useref
    // Get the css
    .pipe(cssFilter)
    .pipe($.if(env.optimize, $.csso()))
    .pipe(cssFilter.restore)
    // Get the custom javascript
    .pipe(jsAppFilter)
    .pipe($.ngAnnotate({ add: true, single_quotes: false }))
    .pipe($.if(env.optimize, $.uglify()))
    .pipe(jsAppFilter.restore)
    // Get the vendor javascript
    .pipe(jslibFilter)
    .pipe($.if(env.optimize, $.uglify())) // another option is to override wiredep to use min files
    .pipe(jslibFilter.restore)
    // Take inventory of the file names for future rev numbers
    .pipe($.if(env.optimize, $.rev()))
    // Apply the concat and file replacement with useref
    .pipe(assets.restore())
    .pipe($.useref())
    // Replace the file names in the html with rev numbers
    .pipe($.if(env.optimize, $.revReplace()))
    .pipe(gulp.dest(env.path));
});

/**
 * Create $templateCache from the html templates
 * @return {Stream}
 */
gulp.task('templatecache', function(done) {

  /*done();
   return;*/
  log('Creating an AngularJS $templateCache');

  var sourceFiles = config.paths.src.templates;
  // // if we're in development mode, don't add any of the partial views to the templateCache
  // // to easier troubleshoot any issue

  if (!env.optimize) {
    //sourceFiles = '[]';
  }


  return gulp
    .src(sourceFiles)
    .pipe($.if(args.verbose, $.bytediff.start()))
    .pipe($.htmlmin({ empty: true, collapseWhitespace: true }))
    .pipe($.if(args.verbose, $.bytediff.stop(bytediffFormatter)))
    .pipe($.angularTemplatecache(
      config.templateCache.file,
      config.templateCache.options
    ))
    .pipe(gulp.dest(config.paths.dest.tmp));
});

/**
 *  Run unit tests
 * */
gulp.task('test', ['clean-tests'], function(done) {
  var bowerDeps = wiredep({
    directory: config.paths.src.bowerFolder,
    exclude: [],
    dependencies: true,
    devDependencies: true
  });

  // This will jQuery to the start so that AngularJS won't use jqLite
  bowerDeps.js.unshift(config.paths.src.bowerFolder + '/jquery/dist/jquery.js');

  return new karmaServer({
    singleRun: true,
    autoWatch: false,
    configFile: __dirname + config.configFiles.karma
  }, done).start();
});

/**
 * Clean coverage folder
 */
gulp.task('clean-tests', function(done) {
  log('Cleaning test coverege folder...');
  return clean('coverage/*', done);
});

/**
 * Run JSHint (Lint) on all js files in the src/app folder
 */
gulp.task('lint', function() {
  return gulp.src(config.paths.src.jsFiles)
             .pipe($.jshint())
             .pipe($.jshint.reporter('jshint-stylish')) // make the results look nice
             .pipe($.jshint.reporter('fail')); // make the task fail it there are errors
});

/////////////////////////////////////////////////////////////////////////////////////
//                              Private helper methods
/////////////////////////////////////////////////////////////////////////////////////

/**
 * Log a message or series of messages using chalk's blue color.
 * Can pass in a string, object or array.
 */
function log(msg) {
  if (typeof (msg) === 'object') {
    for (var item in msg) {
      if (msg.hasOwnProperty(item)) {
        $.util.log($.util.colors.blue(msg[item]));
      }
    }
  } else {
    $.util.log($.util.colors.blue(msg));
  }
}

/**
 * Log an error message and emit the end of a task
 */
function errorLogger(error) {
  log('*** Start of Error ***');
  log(error);
  log('*** End of Error ***');
  this.emit('end');
}

/**
 * Delete all files in a given path
 * @param  {Array}   path - array of paths to delete
 * @param  {Function} done - callback when complete
 */
function clean(path, done) {
  log('Cleaning: ' + $.util.colors.blue(path));
  del.sync(path);
  if (done) {
    done();
  }
}

/**
 * Formatter for bytediff to display the size changes after processing
 * @param  {Object} data - byte data
 * @return {String}      Difference in bytes, formatted
 */
function bytediffFormatter(data) {
  var difference = (data.savings > 0) ? ' smaller.' : ' larger.';
  return data.fileName + ' went from ' +
    (data.startSize / 1000).toFixed(2) + ' kB to ' +
    (data.endSize / 1000).toFixed(2) + ' kB and is ' +
    formatPercent(1 - data.percent, 2) + '%' + difference;
}

/**
 * Format a number as a percentage
 * @param  {Number} num       Number to format as a percent
 * @param  {Number} precision Precision of the decimal
 * @return {String}           Formatted perentage
 */
function formatPercent(num, precision) {
  return (num * 100).toFixed(precision);
}

/**
 * Inject files in a sorted sequence at a specified inject label
 * @param   {Array} src   glob pattern for source files
 * @param   {String} label   The label name
 * @param   {Array} order   glob pattern for sort order of the files
 * @returns {Stream}   The stream
 */
function inject(src, label, order) {
  var options = { relative: true };
  if (label) {
    options.name = 'inject:' + label;
  }

  return $.inject(orderSrc(src, order), options);
}

/**
 * Method that injects the <base tag into head, using the {env.virtualFolder} variable
 */
function injectBase() {
  var options = {
    starttag: '<!-- inject:base -->',
    transform: function(filePath, file) {
      return '<base href="/' + env.virtualFolder + '" />';
    }
  };

  //we use the index file as source, because it's unique, and we need the transform to only be called once
  return $.inject(gulp.src([config.index], { read: false }), options);
}

/**
 * Order a stream
 * @param   {Stream} src   The gulp.src stream
 * @param   {Array} order Glob array pattern
 * @returns {Stream} The ordered stream
 */
function orderSrc(src, order) {
  return gulp
    .src(src, {read: false})
    .pipe($.if(order, $.order(order)));
}

/**
 * wiredep and bower settings
 */
function getWiredepDefaultOptions() {
  var options = {
    bowerJson: require('./bower.json'),
    directory: config.paths.src.bowerFolder
  };
  return options;
};
