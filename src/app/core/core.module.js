/**
 * Created by lticrea on 7/29/2016.
 */
(function() {
  'use strict';

  angular.module('payerPortal.core', [
    'ngAnimate',
    'ngSanitize',
    'ui.router',
    'ngTable',
    'ui.mask',
    'ui.bootstrap',
    'angularSpinners'
  ]);
})();