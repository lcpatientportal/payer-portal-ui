'use strict';

describe('payerPortal.version module', function() {
  beforeEach(module('payerPortal.version'));

  describe('version service', function() {
    it('should return current version', inject(function(version) {
      expect(version).toEqual('0.1');
    }));
  });
});
