'use strict';

angular.module('payerPortal.version', [
  'payerPortal.version.interpolate-filter',
  'payerPortal.version.version-directive'
])

.value('version', '0.1');
