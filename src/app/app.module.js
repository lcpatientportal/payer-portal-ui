(function () {
  'use strict';

  // Declare app level module which depends on views, and components
  angular.module('payerPortal', [
    'ngRoute',
    'payerPortal.core',
    'payerPortal.version'
  ]).
  config(['$locationProvider', '$routeProvider', function($locationProvider, $urlRouterProvider) {

    // enable html5Mode (remove the "#" in the url)
    $locationProvider.html5Mode(true);

    $urlRouterProvider.otherwise("/");
  }]).
  run(["$http", "$location", "$rootScope",
    function ($http, $location, $rootScope) {
      $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
        console.log('StateChanged: from [ ' + from.name + ' ] to [ ' + to.name + ' ]');
        $rootScope.previousState = from.name;
        $rootScope.currentState = to.name;

        $rootScope.title = to.title;
      });
  }]);
})();